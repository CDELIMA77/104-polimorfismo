package com.company;

public class Professor extends Pessoa implements Funcionario,Pobre{
    private String especializacao;

    public Professor(String nome, String sexo, String especializacao) {
        super(nome, sexo);
        this.especializacao = especializacao;
    }

    @Override
    public void acordar() {
        System.out.println("O professor nao queria acordar.");
    }

    @Override
    public void baterPonto() {

    }

    @Override
    public void faltar() {

    }

    @Override
    public void tirarFerias() {

    }

    @Override
    public void trabalhar() {

    }

    @Override
    public void atrasarBoleto() {

    }
}
