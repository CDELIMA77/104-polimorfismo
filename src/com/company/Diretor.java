package com.company;

public class Diretor {
    private boolean rico;
    private double salario;

    public Diretor(boolean rico, double salario) {
        this.rico = rico;
        this.salario = salario;
    }


    @Override
    public String toString() {
        return "Diretor{" +
                "rico=" + rico +
                ", salario=" + salario +
                '}';
    }
}
