package com.company;

public interface Rico {

    public void aplicarNaBolsaParaFicarPobre();
    public void pagarCartaoDeCreditoSemLimite();
    public void viajar();
}
