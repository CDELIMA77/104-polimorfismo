package com.company;

public class Main {

    public static void main(String[] args) {
        Professor professor = new Professor("Vini", "M", "Python");

        Aluno aluno = new Aluno("Lucas", "12312321",
                "1231231", "Java");
        Aluno aluno2 = new Aluno();
        aluno2.setNome("Barbara");
        aluno2.setCpf("12312312");


        Diretor diretor = new Diretor(true, 23413.40);
        Calculadora calc = new Calculadora();

        calc.somar(10, 10.5);
        calc.somar(10.0, 25);
    }
}
