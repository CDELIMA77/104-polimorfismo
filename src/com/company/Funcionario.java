package com.company;

public interface Funcionario {
    public void baterPonto();
    public void faltar();
    public void tirarFerias();
}
