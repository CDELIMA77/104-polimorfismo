package com.company;

public class Vendedor extends Pessoa implements Rico {

    public Vendedor(String nome, String sexo) {
        super(nome, sexo);
    }

    @Override
    public void acordar() {
        System.out.println("Vendedor acordou 6h00");
    }

    @Override
    public void aplicarNaBolsaParaFicarPobre() {
        System.out.println("Fiquei pobre");
    }

    @Override
    public void pagarCartaoDeCreditoSemLimite() {
        System.out.println("Ganhei milhas");
    }

    @Override
    public void viajar() {
        System.out.println("Dubai no FDS");
    }
}
